"use strict";
let userFirstName = prompt("Enter your firstname").trim();
while (userFirstName == null || !userFirstName) {
  userFirstName = prompt("Enter your firstname again").trim();
}
let userLastName = prompt("Enter your lastname").trim();
while (userLastName == null || !userLastName) {
  userLastName = prompt("Enter your lastname again").trim();
}
let user_bd = prompt("Enter your date of birth in format dd.mm.yyyy:", "dd.mm.yyyy").trim();
while (user_bd == 0 || user_bd == null || user_bd.length<10) {
  user_bd = prompt("Enter your date of birth in the format shown dd.mm.yyyy: again", "dd.mm.yyyy").trim();
};
let userBDayArr = user_bd.split(".")
let userBirthDay = new Date(userBDayArr[2], userBDayArr[1]-1, userBDayArr[0]);
function createNewUser(userFirstName, userLastName, userBirthDay) {
  const newUser = {
    firstName: userFirstName,
    lastName: userLastName,
    birthDay: userBirthDay,
    getLogin: () => {
      let login = userFirstName[0] + userLastName;
      return login.toLowerCase()
    },
    getAge: () => {
      let age = Date.now() - userBirthDay.getTime();
      let ageDate = new Date(age);
      let years = Math.abs(ageDate.getUTCFullYear() - 1970);
      return years;
    },
    getPassword: () => {
      let password = userFirstName[0].toUpperCase() + userLastName.toLowerCase() + userBirthDay.getFullYear();
      return password
    },
    setFirstName: newFName => {
      Object.defineProperty(newUser, "firstName", {value: newFName})
    },
    setLastName: newLName => {
      Object.defineProperty(newUser, "lastName", {value: newLName})
    },
  };
  Object.defineProperty(newUser, "firstName", {value: userFirstName, configurable: true, writable: false});
  Object.defineProperty(newUser, "lastName", {value: userLastName, configurable: true, writable: false});
  return newUser;
}
const user = createNewUser(userFirstName, userLastName, userBirthDay);
console.log("firstName: " + user.firstName + ", lastName: " + user.lastName + ", birthday:" + user.birthDay);

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


